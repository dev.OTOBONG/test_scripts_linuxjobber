import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time

class Linuxjobber_WE_APP(unittest.TestCase):
    """"""
    @classmethod
    def setUpClass(cls):
        """Initialize setup for app prerequisites"""
        # Chrome driver object for creating Chrome instance
        serv = Service(r"/home/machines/webdrivers/chromedriver")
        cls.driver = webdriver.Chrome(service=serv)

        # Maximize web browser
        cls.driver.maximize_window()

        # Get and open url specified
        cls.driver.get("https://dev2api.linuxjobber.com/")

        # waits for 30 seconds until all web page elements are available
        cls.driver.implicitly_wait(30)

    def test_welcomPage(self):
        """ Verify trainee's welcome page redirect """

        loginElement = self.driver.find_element(By.XPATH, "/html/body/header/div/div/div/div[2]/div[2]/a[1]").click()

        emailField2 = self.driver.find_element(By.CSS_SELECTOR, "#formGroupExampleInput")
        if emailField2.is_displayed() and emailField2.is_enabled():
            emailField2.send_keys("samsonotobong7@mailinator.com")

        passwordField2 = self.driver.find_element(By.CSS_SELECTOR, "#formGroupExampleInput2")
        if passwordField2.is_displayed() and passwordField2.is_enabled():
           passwordField2.send_keys("Wordpass100..?")

        loginBtnClick = self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
        time.sleep(5)

        work_elem_click = self.driver.find_element(By.CSS_SELECTOR, ".work > a:nth-child(1)").click()
        work_EX_click = self.driver.find_element(By.LINK_TEXT, "Work Experience").click()
        time.sleep(5)

        # Skip first task
        skip_task = self.driver.find_element(By.CSS_SELECTOR, ".btn-sm").click()
        reason_for_skip = self.driver.find_element(By.ID, "reason")
        reason_for_skip.send_keys("I'm skipping this task due to logistic reasons")
        confirmBtnclick = self.driver.find_element(By.ID, "skipTask").click()
        time.sleep(10)

        # Skip second task
        skip_second_task = self.driver.find_element(By.CSS_SELECTOR, ".btn-sm").click()
        reason_for_skip = self.driver.find_element(By.ID, "reason")
        reason_for_skip.send_keys("I'm skipping this task due to logistic reasons")
        confirmBtnclick = self.driver.find_element(By.ID, "skipTask").click()
        time.sleep(10)

        # Skip third task
        skip_third_task = self.driver.find_element(By.CSS_SELECTOR, ".btn-sm").click()
        reason_for_skip = self.driver.find_element(By.ID, "reason")
        reason_for_skip.send_keys("I'm skipping this task due to logistic reasons")
        confirmBtnclick = self.driver.find_element(By.ID, "skipTask").click()
        self.driver.refresh()
        time.sleep(10)



    @classmethod
    def tearDownClass(cls):
        cls.driver.close()

if __name__ == "__name__":
    unittest.main()
