# Imports for necessary Selenium Python bindings
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import time

class Linuxjobber_WE_APP(unittest.TestCase):
    """"""
    @classmethod
    def setUpClass(cls):
        """Initialize setup for app prerequisites"""
        # Chrome driver object for creating Chrome instance
        serv = Service(r"/home/machines/webdrivers/chromedriver")
        cls.driver = webdriver.Chrome(service=serv)

        # Maximize web browser
        cls.driver.maximize_window()

        # Get and open url specified
        cls.driver.get("https://dev2api.linuxjobber.com/")

        # waits for 30 seconds until all web page elements are available
        cls.driver.implicitly_wait(30)

    def test_enrollment(self):
        """ verify Work experience trainee enrollment. """
        loginElement = self.driver.find_element(By.XPATH, "/html/body/header/div/div/div/div[2]/div[2]/a[1]").click()

        emailField2 = self.driver.find_element(By.CSS_SELECTOR, "#formGroupExampleInput")
        if emailField2.is_displayed() and emailField2.is_enabled():
            emailField2.send_keys("samsonotobong7@mailinator.com")

        passwordField2 = self.driver.find_element(By.CSS_SELECTOR, "#formGroupExampleInput2")
        if passwordField2.is_displayed() and passwordField2.is_enabled():
           passwordField2.send_keys("Wordpass100..?")

        loginBtnClick = self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
        time.sleep(5)

        work_elem_click = self.driver.find_element(By.CSS_SELECTOR, ".work > a:nth-child(1)").click()
        work_EX_click = self.driver.find_element(By.LINK_TEXT, "Work Experience").click()
        enrollBtnClck = self.driver.find_element(By.CSS_SELECTOR, ".sign-in-btn").click()
        time.sleep(5)

        select = self.driver.find_element(By.CSS_SELECTOR, "#stripejsfilemain > div:nth-child(2) > select:nth-child(4)")
        dropDown = Select(select)
        dropDown.select_by_visible_text("Test")
        continueBtnClick = self.driver.find_element(By.CSS_SELECTOR, ".we_submit_btn").click()
        time.sleep(5)

        WebDriverWait(self.driver, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR, "#card_number"))).click()
        WebDriverWait(self.driver, 20).until(EC.frame_to_be_available_and_switch_to_it((By.CSS_SELECTOR, "iframe[src='https://checkout.stripe.com/v3/JdhudoUwvSFS85kPDZEQ.html?distinct_id=86a73dfd-3814-3f85-6558-144d81b25a48']"))).send_keys("4242424242424242")

        month_year = self.driver.find_element(By.ID, "cc-exp")
        if month_year.is_displayed() and month_year.is_enabled():
            month_year.send_keys("01/23")

        cvc = self.driver.find_element(By.ID, "cc-csc")
        if cvc.is_displayed() and cvc.is_enabled():
            cvc.send_keys("123")

        payBtnClick = self.find_element(By.CSS_SELECTOR, ".iconTick").click()
        time.sleep(5)

    @classmethod
    def tearDownClass(cls):
        """Disposes all initial setup"""
        cls.driver.close()

if __name__ == "__main__":
    unittest.main()