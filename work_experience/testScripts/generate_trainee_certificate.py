import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time

class Linuxjobber_WE_APP(unittest.TestCase):
    """"""
    @classmethod
    def setUpClass(cls):
        """Initialize setup for app prerequisites"""
        # Chrome driver object for creating Chrome instance
        serv = Service(r"/home/machines/webdrivers/chromedriver")
        cls.driver = webdriver.Chrome(service=serv)

        # Maximize web browser
        cls.driver.maximize_window()

        # Get and open url specified
        cls.driver.get("https://dev2api.linuxjobber.com/")

        # waits for 30 seconds until all web page elements are available
        cls.driver.implicitly_wait(30)

    def test_generate_trainee_certificate(self):
        """ verify trainee's certificate generation after programme completion """

        loginElement = self.driver.find_element(By.XPATH, "/html/body/header/div/div/div/div[2]/div[2]/a[1]").click()

        emailField2 = self.driver.find_element(By.CSS_SELECTOR, "#formGroupExampleInput")
        if emailField2.is_displayed() and emailField2.is_enabled():
            emailField2.send_keys("samsonotobong7@mailinator.com")

        passwordField2 = self.driver.find_element(By.CSS_SELECTOR, "#formGroupExampleInput2")
        if passwordField2.is_displayed() and passwordField2.is_enabled():
           passwordField2.send_keys("Wordpass100..?")

        loginBtnClick = self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
        time.sleep(5)

        work_elem_click = self.driver.find_element(By.CSS_SELECTOR, ".work > a:nth-child(1)").click()
        work_EX_click = self.driver.find_element(By.LINK_TEXT, "Work Experience").click()
        time.sleep(5)

        link_to_certificate = self.driver.find_element(By.CSS_SELECTOR, ".congrats-para > p:nth-child(2) > a:nth-child(1)")
        link_to_certificate.click()
        time.sleep(30)


    @classmethod
    def tearDownClass(cls):
        cls.driver.close()

if __name__ == "__name__":
    unittest.main()
