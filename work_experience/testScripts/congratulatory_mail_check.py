import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time

class Linuxjobber_WE_APP(unittest.TestCase):
    """"""
    @classmethod
    def setUpClass(cls):
        """Initialize setup for app prerequisites"""
        # Chrome driver object for creating Chrome instance
        serv = Service(r"/home/machines/webdrivers/chromedriver")
        cls.driver = webdriver.Chrome(service=serv)

        # Maximize web browser
        cls.driver.maximize_window()

        # Get and open url specified
        cls.driver.get("https://www.mailinator.com/v4/public/inboxes.jsp")

        # waits for 30 seconds until all web page elements are available
        cls.driver.implicitly_wait(30)

    def test_congratulatory_mail(self):
        """ Verify if trainee receives congratulatory mail """
        emailID = self.driver.find_element(By.CSS_SELECTOR, "#inbox_field")
        if emailID.is_displayed() and emailID.is_enabled():
            emailID.send_keys("samsonotobong7")

        GoBtnClick = self.driver.find_element(By.CSS_SELECTOR, ".primary-btn").click()
        time.sleep(10)


    @classmethod
    def tearDownClass(cls):
        cls.driver.close()

if __name__ == "__name__":
    unittest.main()
