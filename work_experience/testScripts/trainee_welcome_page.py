""" Import trainee enrollment function to replace login """
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import time

class Linuxjobber_WE_APP(unittest.TestCase):
    """"""
    @classmethod
    def setUpClass(cls):
        """Initialize setup for app prerequisites"""
        # Chrome driver object for creating Chrome instance
        serv = Service(r"/home/machines/webdrivers/chromedriver")
        cls.driver = webdriver.Chrome(service=serv)

        # Maximize web browser
        cls.driver.maximize_window()

        # Get and open url specified
        cls.driver.get("https://dev2api.linuxjobber.com/")

        """ Verify web page here........................"""

        # waits for 30 seconds until all web page elements are available
        cls.driver.implicitly_wait(30)

    def test_welcomPage(self):
        loginElement = self.driver.find_element(By.XPATH, "/html/body/header/div/div/div/div[2]/div[2]/a[1]").click()

        emailField2 = self.driver.find_element(By.CSS_SELECTOR, "#formGroupExampleInput")
        if emailField2.is_displayed() and emailField2.is_enabled():
            emailField2.send_keys("samsonotobong7@mailinator.com")

        passwordField2 = self.driver.find_element(By.CSS_SELECTOR, "#formGroupExampleInput2")
        if passwordField2.is_displayed() and passwordField2.is_enabled():
           passwordField2.send_keys("Wordpass100..?")

        loginBtnClick = self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
        time.sleep(5)

        work_elem_click = self.driver.find_element(By.CSS_SELECTOR, ".work > a:nth-child(1)").click()
        work_EX_click = self.driver.find_element(By.LINK_TEXT, "Work Experience").click()
        time.sleep(5)

        # ********* WATCH VIDEO TO COMPLETE ENROLLMENT PROCESS ****************
        vidpayElem = self.driver.find_element(By.CSS_SELECTOR, "#playss").click()
        time.sleep(10)

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()

if __name__ == "__name__":
    unittest.main()
