# Imports for necessary Selenium Python bindings
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time

class Linuxjobber_WE_APP(unittest.TestCase):
    """"""
    @classmethod
    def setUpClass(cls):
        """Initialize setup for app prerequisites"""
        # Chrome driver object for creating Chrome instance
        serv = Service(r"/home/machines/webdrivers/chromedriver")
        cls.driver = webdriver.Chrome(service=serv)

        # Maximize web browser
        cls.driver.maximize_window()

        # Get and open url specified
        cls.driver.get("https://dev2api.linuxjobber.com/")

        # waits for 30 seconds until all web page elements are available
        cls.driver.implicitly_wait(30)

    def test_signUp(self):
        """ verify signup functionality on linuxjobber """
        signUpBtnclick = self.driver.find_element(By.CSS_SELECTOR, ".active")
        signUpBtnclick.click()

        # Verify full name
        fullNameField = self.driver.find_element(By.NAME, "fullname")
        if fullNameField.is_displayed() and fullNameField.is_enabled():
            fullNameField.send_keys("Tester Sam")

        # Verify email
        emailField = self.driver.find_element(By.NAME, "email")
        if emailField.is_displayed() and emailField.is_enabled():
            emailField.send_keys("samsonotobong7@mailinator.com")

        # Verify password
        passwordField = self.driver.find_element(By.NAME, "password")
        if passwordField.is_displayed() and passwordField.is_enabled():
            passwordField.send_keys("Wordpass100..?")

        # Verify Create account submit button click
        createAccount = self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
        time.sleep(10)

    @classmethod
    def tearDownClass(cls):
        """Disposes all initial setup"""
        cls.driver.close()

if __name__ == "__main__":
    unittest.main()
